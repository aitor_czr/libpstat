/*
   libpstat: a library for getting information about running processes
   Copyright (C) 2014  Jude Nelson

   This program is dual-licensed: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License version 3 or later as 
   published by the Free Software Foundation. For the terms of this 
   license, see LICENSE.LGPLv3+ or <http://www.gnu.org/licenses/>.

   You are free to use this program under the terms of the GNU Lesser General
   Public License, but WITHOUT ANY WARRANTY; without even the implied 
   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   See the GNU Lesser General Public License for more details.

   Alternatively, you are free to use this program under the terms of the 
   Internet Software Consortium License, but WITHOUT ANY WARRANTY; without
   even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
   For the terms of this license, see LICENSE.ISC or 
   <http://www.isc.org/downloads/software-support-policy/isc-license/>.
*/

#include "libpstat.h"

#ifdef _LINUX
#include "os/linux.h"
#endif

#ifdef _UNKNOWN
#error "Undefined OS target"
#endif

// insert more OSs here

uint64_t pstat_supported_features() {
   return pstat_os_supported_features();
}

// allocate a new pstat 
struct pstat* pstat_new() {
   return (struct pstat*)calloc( sizeof(struct pstat), 1 );
}

// free a pstat 
void pstat_free( struct pstat* ps ) {
   free( ps );
}

// get the status of a running processes
// return 0 on success 
// return negative on error
int pstat( pid_t pid, struct pstat* ps, int flags ) {
   return pstat_os( pid, ps, flags );
}

pid_t pstat_get_pid( struct pstat* ps ) {
   return ps->pid;
}

bool pstat_is_running( struct pstat* ps ) {
   return ps->running;
}

bool pstat_is_deleted( struct pstat* ps ) {
   return ps->deleted;
}

int pstat_get_path( struct pstat* ps, char* pathbuf ) {
   
   if( pathbuf != NULL ) {
      strcpy( pathbuf, ps->path );
   }
   
   return (int)strlen( ps->path );
}
   
int pstat_get_stat( struct pstat* ps, struct stat* sb ) {
   
   if( !ps->running ) {
      return -ENODATA;
   }
   
   memcpy( sb, &ps->bin_stat, sizeof(struct stat) );
   
   return 0;
}

uint64_t pstat_get_starttime( struct pstat* ps ) {
   
   return ps->starttime;
}

// Returned array must be freed to prevent memory leaks
int pstat_get_binary_path_from_string(char **buf, int sz, const char *pid_value)
{
   pid_t pid = 0;
   int rc = 0; 
   
   struct stat bin_stat;
   
   struct pstat* ps = pstat_new();
   if( ps == NULL )
      exit(EXIT_FAILURE);
   
   rc = sscanf( pid_value, "%d", &pid );
   if( rc != 1 ) {
     fprintf( stderr, "sscanf(%d) rc = %d\n", pid, rc );
     exit(EXIT_FAILURE);
   }
      
   rc = pstat( pid, ps, 0 );
      
   if( rc != 0 ) {
     fprintf( stderr, "pstat(%d) rc = %d\n", pid, rc );
     exit(EXIT_FAILURE);
   }
   
   if( !pstat_is_running(ps) )
      return 1;
   
   if( !pstat_is_deleted(ps) ) {
   
      *buf = (char*)malloc(sz * sizeof(char));
      if(*buf == NULL) {
        fprintf( stderr, "Memory allocation failure: %d\n", rc );
        exit(EXIT_FAILURE);
      }
      
      if((int)strlen(ps->path) > sz) {
		 pstat_free(ps); 
         return 1;
      }
      pstat_get_stat( ps, &bin_stat );
      strcpy(*buf, ps->path);
   }
   
   pstat_free( ps );
	
   return 0;
}

int pstat_get_binary_path_from_integer(char **buf, int sz, pid_t pid)
{
   int rc = 0; 
   
   struct stat bin_stat;
   
   struct pstat* ps = pstat_new();   
   if( ps == NULL ) {
      exit(EXIT_FAILURE);
   }
      
   rc = pstat( pid, ps, 0 );      
   if( rc != 0 ) {
     fprintf( stderr, "pstat(%d) rc = %d\n", pid, rc );
     exit(EXIT_FAILURE);
   }
   
   if( !pstat_is_running(ps) )
      return 1;
   
   if( !pstat_is_deleted(ps) ) {
   
      *buf = (char*)malloc(sz * sizeof(char));
      if(*buf == NULL) {
        fprintf( stderr, "Memory allocation failure: %d\n", rc );
        exit(EXIT_FAILURE);
      }
      
      if((int)strlen(ps->path) > sz) {
		 pstat_free(ps); 
         return 1;
      }
      pstat_get_stat(ps, &bin_stat);
      strcpy(*buf, ps->path);
   }
   
   pstat_free( ps );
	
   return 0;
}

uint64_t pstat_starttime_from_integer(pid_t pid)
{
   int rc = 0;
   uint64_t start_time = 0; 
   
   struct pstat* ps = pstat_new();   
   if( ps == NULL ) {
      exit(EXIT_FAILURE);
   }
      
   rc = pstat( pid, ps, 0 );      
   if( rc != 0 ) {
     fprintf( stderr, "pstat(%d) rc = %d\n", pid, rc );
     exit(EXIT_FAILURE);
   }
   
   if( !pstat_is_deleted( ps ) && pstat_is_running( ps ) ) {
   
      start_time = ps->starttime;
   }
   
   pstat_free( ps );
	
   return start_time;
}
